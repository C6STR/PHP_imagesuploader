<?php
session_start();
include("../Model/db_model.php");//DB操作読み込み
//id,pass、POSTから格納
$login_id=htmlspecialchars($_POST["login_id"]);
$pass=htmlspecialchars($_POST["pass"]);
//db参照
$row=login($login_id);
//passとidの比較
if(password_verify($pass,$row["password_hash"]) && $pass!=""){
  echo "complete";
  header('Location: ../View/menu.php');
  $_SESSION["name"]=$row["name"];
  $_SESSION["login_id"]=$row["login_id"];
  $_SESSION["id"]=$row["id"];
}else{
  //エラーにリダイレクト
  header('Location: ../View/login.php');
  $_SESSION["error_message"]="ログインIDまたはパスワードを見直してください";
}
 ?>
