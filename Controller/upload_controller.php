<?php
session_start();
include("../Model/db_model.php");//DB操作読み込み
$file_name_get=$_FILES["upload"]["name"];
$title=$_POST["title"];
$id=$_SESSION["id"];
if(is_uploaded_file($_FILES["upload"]["tmp_name"])){
  $file_number=file_number();
  $file_name=$file_number."_".$file_name_get;
  if(move_uploaded_file($_FILES["upload"]["tmp_name"],"../Images/".$file_name)){
    image_upload($id,$title,$file_name);
    $_SESSION["upload_msg"]="アップロード完了";
    header('Location: ../View/upload.php');
  }else{
    $_SESSION["upload_msg"]="エラー";
    header('Location: ../View/upload.php');
  }
}else{
  $_SESSION["upload_msg"]="ファイルが選択されていません";
  header('Location: ../View/upload.php');
}
 ?>
