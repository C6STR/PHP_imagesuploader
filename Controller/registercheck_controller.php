<?php
session_start();
include("../Model/db_model.php");//DB操作読み込み
//id,pass、POSTから格納
$login_id=htmlspecialchars($_POST["login_id"]);
$pass=htmlspecialchars($_POST["pass"]);
$name=htmlspecialchars($_POST["name"]);
//配列に文字数
$length=array (mb_strlen($login_id),mb_strlen($pass),mb_strlen($name) );
$length_flag=0;
for($i=0;$i<3;$i++){
  if($length[$i]<3){
    $length_flag=1;
  }
}
if($length_flag==0){
  $pass_hash=password_hash($pass,PASSWORD_DEFAULT);
  $exists=exists_check($login_id);//IDがすでに存在していないか
  if($exists==0){
    regist($login_id,$pass_hash,$name);//登録
    header('Location: ../index.html');
  }else{
    $_SESSION["error_message"]="すでに登録されているIDです。";
    header('Location: ../View/register.php');
  }
}else{
  $_SESSION["error_message"]="正しい値を入れてください";
  header('Location: ../View/register.php');
}
  ?>
